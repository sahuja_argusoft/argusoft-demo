import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MustMatch } from './passwordValidator';

@Component({
  selector: 'app-personal-details',
  templateUrl: './personal-details.component.html',
  styleUrls: ['./personal-details.component.sass']
})
export class PersonalDetailsComponent implements OnInit {

  personalDetailsForm: FormGroup;
  submitted = false;

  constructor(
    private formBuilder: FormBuilder
  ) { }

  ngOnInit() {
    this.initForm();
  }

  initForm() {
    this.personalDetailsForm = this.formBuilder.group({
      first_name: [null, [Validators.required]],
      last_name: [null, [Validators.required]],
      email: [null, [Validators.required, Validators.email]],
      phone: [null, [Validators.required]],
      password: [null, [Validators.required, Validators.minLength(8)]],
      confirm_password: [null, [Validators.required, Validators.minLength(8)]],
      age: [null, [Validators.required, Validators.min(0)]]
    }, {
      validator: MustMatch('password', 'confirm_password')
    });
  }

  submitObject() {
    return {
      first_name: this.getFormControl('first_name'),
      last_name: this.getFormControl('last_name'),
      email: this.getFormControl('email'),
      phone: this.getFormControl('phone'),
      password: this.getFormControl('password'),
      age: this.getFormControl('age')
    };
  }

  getFormControl(key) {
    return this.personalDetailsForm.get(key).value;
  }

  onSubmit() {
    this.submitted = true;
    if (!this.personalDetailsForm.invalid) {
      console.log(this.submitObject());
    }
  }
}
