import { Component, OnInit } from '@angular/core';
import { CommonService } from './../../../Shared/common.service';
import { Options } from 'ng5-slider';
@Component({
  selector: 'app-loan-calculator',
  templateUrl: './loan-calculator.component.html',
  styleUrls: ['./loan-calculator.component.sass']
})
export class LoanCalculatorComponent implements OnInit {

  principal: number = 0;
  rate: number = 0;
  term: number = 0;
  termFormat = 'year'; // year | month
  loanMonthlyPayment: any;

  principleAmountOptions: Options = {
    floor: 0,
    ceil: 500000,
    tickStep: 100000,
    showTicks: true,
    showTicksValues: true,
    translate: (value: number): string => { return (value > 0)  ? ('$' + value/1000+'K') : '0' },
  };
  interestRateOptions: Options = {
    floor: 0,
    ceil: 20,
    tickStep: 5,
    showTicks: true,
    showTicksValues: true,
  };
  termOptionsYear: Options = {
    floor: 0,
    ceil: 40,
    tickStep: 10,
    showTicks: true,
    showTicksValues: true,
  };
  termOptionsMonth: Options = {
    floor: 0,
    ceil: 480,
    tickStep: 120,
    showTicks: true,
    showTicksValues: true,
  }

  constructor(private commonService: CommonService) {}

  ngOnInit() {}

  addNotify() {
    if (this.principal && this.rate && this.term) {
      let {payment, term} = this.loanPayment();
      this.commonService.notifyChange({
        principal: this.principal,
        rate: this.rate,
        term: term,
        loanMonthlyPayment: payment
      });
    }
  }

  loanPayment() {

    //Can use a whole number percentage or decimal
    let rate = this.rate;
    let term = this.term;
    if (this.rate > 1) {
      rate = this.rate * 0.01;
    } else {
      rate = this.rate;
    }
    //Can accept term in years or months
    term = term * (this.termFormat === 'year' ? 12 : 1 );
    let monthlyRate = rate / 12;
    let factor = Math.pow(monthlyRate + 1, term);
    let numerator = monthlyRate * factor;
    let denominator = factor - 1;
    let quotient = numerator / denominator;
    let payment = this.principal * quotient;

    console.log("Loan amount is: " + payment.toFixed(0));
    return {
      payment: Math.round(payment),
      term: term
    };
  };

  decrementValue(reference, value) {
    this[reference] = (this[reference] - value) >= 0 ? this[reference] - value : 0 ;
  }

  incrementValue(reference, value, maxValue) {
    this[reference] = (this[reference] + value) <= maxValue ? this[reference] + value : maxValue ;
  }
  
}
