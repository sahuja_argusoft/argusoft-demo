import { Component, OnInit } from '@angular/core';
import { Chart, ChartOptions} from 'chart.js';
import {Label} from 'ng2-charts';
import { Subscription } from 'rxjs';
import { CommonService } from './../../../Shared/common.service';

@Component({
  selector: 'app-chart',
  templateUrl: './chart.component.html',
  styleUrls: ['./chart.component.sass']
})
export class ChartComponent implements OnInit {
  myPieChart: any;
  loanDataSubscription: Subscription;
  principal: number = 0;
  rate: any= 0;
  term: any = 0;
  loanMonthlyPayment: any = 0;
  totalInterest: number = 0;
  totalPayment:number = 0;

  public doughnutChartLabels: Label[] = ['Principal Amount', 'Total Interest'];
  public doughnutChartData: any;
  public doughnutOption: ChartOptions;

  constructor(private commonService: CommonService) {
    this.loanDataSubscription = this.commonService.notifyObservable$.subscribe((data) => {
      this.principal = data.principal;
      this.rate = data.rate;
      this.term = data.term;
      this.loanMonthlyPayment = data.loanMonthlyPayment;

      if (this.loanMonthlyPayment) {
        this.totalInterest = (this.loanMonthlyPayment * this.term) - this.principal;
        this.totalPayment = this.totalInterest + this.principal
        this.pieChart();
      }
    });
  }

  ngOnInit() {
  }

  pieChart() {
    this.myPieChart = new Chart("doughnutChartLoan", {
      type: 'doughnut',
      data: {
        datasets: [{
          data: [this.principal, this.totalInterest],
          backgroundColor: [
              'rgb(61,186,215)',
              'rgb(242,113,82)'
          ],
          label: ''
        }]
      },
      options: {
        tooltips: {
          enabled: false
        },
        hover: {mode: null},
      },
    });
  }

  ngOnDestroy() {
    this.loanDataSubscription.unsubscribe();
  }
}
