import { Injectable } from "@angular/core";
import { Subject, BehaviorSubject } from "rxjs";
@Injectable({
  providedIn: "root"
})
export class CommonService {
  private notify = new Subject<any>();

  /**
   * Observable string streams
   */
  notifyObservable$ = this.notify.asObservable();

  constructor() {}
  public notifyChange(data = null) {
    this.notify.next(data);
  }
}
